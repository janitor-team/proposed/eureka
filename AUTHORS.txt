
EUREKA AUTHORS
==============

DEVELOPER:

Andrew Apted  <ajapted@users.sf.net>

I created Eureka (based on an existing program) and continue to develop it.


CONTRIBUTORS:

Ioan Chera
  * MacOS X port

Fabian Greffrath
  * Debian packager

Wesley Werner
  * User Manual

Jason R. Johnston (fiftyoars.com)
  * Eureka Logo

Wesley Johnson
  * Doom Legacy definition file


EARLIER WORK:

André Majorel created the Yadex editor.  Eureka began as a fork of
this editor, version 1.7.0 to be precise.

Raphael Quinet and Brendon Wyber created DEU 5.21, which Yadex was
derived from.

Both Yadex and DEU had many contributors, a comprehensive list of them
can be found in the Yadex documentation.


ACKNOWLEDGEMENTS:

Eureka uses the FLTK widget library (http://www.fltk.org)

